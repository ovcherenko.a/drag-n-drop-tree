import { useRef, useState, DragEvent, MutableRefObject } from "react";
import { IData } from "./../types/dataTypes";

interface ItemParams {
  [x: string]: any;
  itemI: number;
}

export default function useDragNDropList({ data }: IData) {
  const [list, setList] = useState(data);
  const [dragging, setDragging] = useState(false);
  const [draggingObj, setDraggingObj] = useState(null);
  const [dragOnObj, setDragOnObj] = useState(false);
  const [dragOnAddObj, setDragOnAddObj] = useState(false);

  const dragItem: MutableRefObject<undefined | ItemParams> = useRef();
  const dragNode: MutableRefObject<undefined | ItemParams | EventTarget> =
    useRef();

  const handleDragStart = (
    e: DragEvent<HTMLDivElement>,
    params: ItemParams
  ) => {
    // e.stopPropagation();
    console.log("handleDragStart params --> ", params);

    setDraggingObj(findItemFromItemI(params.itemI, list));
    console.log("dragItem --->", dragItem);
    dragItem.current = params;
    dragNode.current = e.target;
    dragNode.current &&
      dragNode.current.addEventListener("dragend", handleDragEndInner);
    setTimeout(() => {
      setDragging(true);
    }, 0);
  };

  const handleDragEnter = (
    e: DragEvent<HTMLDivElement>,
    params: ItemParams
  ) => {
    e.stopPropagation();

    if (e.target !== dragNode.current) {
      setList((oldState) => {
        let newList = JSON.parse(JSON.stringify(oldState));
        const targetItem = findItemFromItemI(params.itemI, newList);
        setDragOnObj(targetItem);
        return newList;
      });
    }
  };

  const handleDragEndInner = () => {
    dragNode.current &&
      dragNode.current.removeEventListener("dragend", handleDragEndInner);
    dragItem.current = undefined;
    dragNode.current = undefined;
    setDragging(false);
  };

  const handleDragLeave = () => {
    setDragOnObj(false);
  };

  const handleDragEnd = () => {
    console.log("drag end");

    dragOnObj &&
      setList((oldState) => {
        let newList = JSON.parse(JSON.stringify(oldState));
        let editedArrList = handleArrList(newList, dragOnObj, draggingObj);
        return editedArrList;
      });
    setDragOnAddObj(false);
  };

  // const handleDragOver = (e: DragEvent<HTMLDivElement>) => {
  //   e.preventDefault();
  // };

  const findItemFromItemI = (itemI, list) => {
    let itemIArr = itemI.split("");
    return itemIArr.reduce((prev, curr, currentIndex) => {
      if (currentIndex === 0) {
        return list[curr];
      }
      return prev.children?.[curr];
    }, list);
  };

  const handleArrDelete = (arrList, propItem) => {
    // console.log("handleArrDelete inside");
    arrList.forEach((item, index) => {
      deleteItemFronArr(arrList, item, propItem, index);
      if (item.children) {
        handleArrDelete(item.children, propItem);
      }
    });
    return arrList;
  };

  const deleteItemFronArr = (arrList, item, comparedItem, index) => {
    if (item.id === comparedItem.id) {
      arrList.splice(index, 1);
    }
  };

  let shouldISearchMore = true;

  const handleArrAdding = (arrList, propItem, propItemToBeAdded) => {
    shouldISearchMore &&
      arrList.forEach((item, index) => {
        shouldISearchMore &&
          addingItemToArr(arrList, item, propItem, propItemToBeAdded, index);
        if (item.children) {
          shouldISearchMore &&
            handleArrAdding(item.children, propItem, propItemToBeAdded);
        }
      });
    return arrList;
  };

  const addingItemToArr = (
    arrList,
    item,
    targetItem,
    objectToBeAdded,
    index
  ) => {
    if (item.id === targetItem.id) {
      arrList.splice(index, 0, objectToBeAdded);
      shouldISearchMore = false;
    }
  };

  const handleArrAddingToChild = (arrList, propItem, propItemToBeAdded) => {
    shouldISearchMore &&
      arrList.forEach((item, index, arrListInner) => {
        addingItemToArrAsChild(
          arrListInner,
          item,
          propItem,
          propItemToBeAdded,
          index
        );
        if (item.children) {
          handleArrAddingToChild(item.children, propItem, propItemToBeAdded);
        }
      });
    setDragOnAddObj(false);
    return arrList;
  };

  const addingItemToArrAsChild = (
    arrListInner,
    item,
    targetItem,
    objectToBeAdded,
    index
  ) => {
    if (item.id === targetItem.id) {
      targetItem.children
        ? arrListInner[index].children.splice(index, 0, objectToBeAdded)
        : (() => {
            arrListInner[index].children = [];
            arrListInner[index].children.push(objectToBeAdded);
          })();
    }
  };

  const handleArrList = (arrList, targetItem, draggingObj) => {
    if (targetItem.id !== draggingObj.id) {
      arrList = handleArrDelete(arrList, draggingObj);

      if (dragOnAddObj) {
        arrList = handleArrAddingToChild(arrList, targetItem, draggingObj);
      } else {
        arrList = handleArrAdding(arrList, targetItem, draggingObj);
      }
    }
    shouldISearchMore = true;
    return arrList;
  };

  const handleDragEnterChildrenBtn = () => {
    // console.log("handleDragEnterChildrenBtn");
    setDragOnAddObj(true);
  };

  const handleDragLeaveChildrenBtn = () => {
    // console.log("handleDragLeaveChildrenBtn");
    setDragOnAddObj(false);
  };

  return {
    list,
    dragging,
    handleDragStart,
    handleDragEnter,
    // handleDragOver,
    handleDragLeave,
    handleDragEnd,
    handleDragEnterChildrenBtn,
    handleDragLeaveChildrenBtn,
  };
}
