import useDragNDropList from "./../hooks/useDragNDropList";
import { IData } from "./../types/dataTypes";
import { FC, useEffect, useState } from "react";

const DragNdrop = ({ data }: IData) => {
  const {
    list,
    dragging,
    handleDragStart,
    handleDragEnter,
    handleDragLeave,
    handleDragEnd,
    handleDragEnterChildrenBtn,
    handleDragLeaveChildrenBtn,
  } = useDragNDropList({
    data,
  });

  const [listState, useListState] = useState(list);

  useEffect(() => {
    // console.log("dragNdrop state ---> ", list);
    useListState(list);
  }, [list]);

  return (
    <div className="drag-n-drop">
      {listState.map((item, itemI) => {
        return (
          <div className="dnd-group" key={JSON.stringify(item)}>
            <ListItem
              key={JSON.stringify(item)}
              item={item}
              onDragStartInner={handleDragStart}
              onDragEnterInner={dragging ? handleDragEnter : undefined}
              onDragLeaveInner={handleDragLeave}
              onDragEndInner={handleDragEnd}
              handleDragEnterChildrenBtn={handleDragEnterChildrenBtn}
              handleDragLeaveChildrenBtn={handleDragLeaveChildrenBtn}
              currentItemI={itemI}
            />
          </div>
        );
      })}
    </div>
  );
};

interface IListItemProps {
  item: { title: string; children?: { title: string } };
  indexNum?: string;
}

const ListItem: FC<IListItemProps> = ({
  item,
  onDragStartInner,
  onDragEnterInner,
  onDragEndInner,
  onDragLeaveInner,
  indexNum,
  currentItemI,
  handleDragEnterChildrenBtn,
  handleDragLeaveChildrenBtn,
}) => {
  return (
    <div
      draggable
      className={"dnd-item"}
      onDragStart={
        onDragStartInner
          ? (e) => {
              onDragStartInner(e, {
                itemI: `${currentItemI}`,
              });
            }
          : undefined
      }
      onDragEnter={
        onDragEnterInner
          ? (e) => {
              e.stopPropagation();
              e.nativeEvent.stopImmediatePropagation();
              onDragEnterInner(e, {
                itemI: `${currentItemI}`,
              });
            }
          : undefined
      }
      onDragEnd={onDragEndInner}
      onDragLeave={onDragLeaveInner}
      onDragOver={(e) => {
        e.preventDefault();
      }}
    >
      <p className={"dnd-item"} style={{ pointerEvents: "none" }}>
        {item.title}
      </p>
      <div
        className={"dnd-item-plus"}
        onDragEnter={handleDragEnterChildrenBtn}
        onDragLeave={handleDragLeaveChildrenBtn}
      >
        Drop item here to add children{" "}
      </div>
      {item.children ? (
        <div className="dnd-item-wrapper">
          {item.children &&
            item.children!.map((child, indexChild) => {
              return (
                <div key={JSON.stringify(child)}>
                  <ListItem
                    item={child}
                    indexNum={
                      indexNum
                        ? `${indexNum}${indexChild}`
                        : `${currentItemI}${indexChild}`
                    }
                    onDragStartInner={(e) => {
                      e.stopPropagation();
                      e.nativeEvent.stopImmediatePropagation();
                      console.log("start event");
                      console.log("drag start inner");
                      console.log(`drag start 1 ${indexNum}${indexChild}`);
                      console.log(`drag start 2 ${currentItemI}${indexChild}`);
                      onDragStartInner(e, {
                        itemI: indexNum
                          ? `${indexNum}${indexChild}`
                          : `${currentItemI}${indexChild}`,
                      });
                    }}
                    onDragEnterInner={(e) => {
                      e.stopPropagation();
                      e.nativeEvent.stopImmediatePropagation();
                      onDragEnterInner(e, {
                        itemI: indexNum
                          ? `${indexNum}${indexChild}`
                          : `${currentItemI}${indexChild}`,
                        last: true,
                      });
                    }}
                    handleDragEnterChildrenBtn={handleDragEnterChildrenBtn}
                    handleDragLeaveChildrenBtn={handleDragLeaveChildrenBtn}
                  />
                </div>
              );
            })}
        </div>
      ) : null}
    </div>
  );
};

export default DragNdrop;
