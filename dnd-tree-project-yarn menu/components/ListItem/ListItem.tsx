import { FC } from "react";

interface IProps {
  item: { title: string };
  //   dragStart: () => void;
  //   dragEnter: () => void | undefined;
}

const ListItem: FC<IProps> = ({ item }) => {
  // console.log("item ---> ", item);
  return (
    <div draggable className={"dnd-item"}>
      <p>{item.title}</p>
    </div>
  );
};

export default ListItem;
