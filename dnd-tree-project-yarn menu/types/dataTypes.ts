export interface IData {
  data: { title: string; children?: { title: string }[] }[];
}
