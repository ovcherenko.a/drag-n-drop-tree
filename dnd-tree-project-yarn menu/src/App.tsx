import { useState } from "react";
import DragNDrop from "./../components/DragNDrop";
import "./App.css";

const menuData = [
  // {
  //   id: "g0",
  //   title: "children0",
  //   children: [
  //     { id: "g00", title: "children00" },
  //     { id: "g01", title: "children01" },
  //   ],
  // },
  // {
  //   id: "g1",
  //   title: "children1",
  //   children: [
  //     { id: "g10", title: "children10" },
  //     {
  //       id: "g11",
  //       title: "children11",
  //       children: [
  //         { id: "g110", title: "children110" },
  //         {
  //           id: "g111",
  //           title: "children111",
  //           children: [
  //             { id: "g1110", title: "children1110" },
  //             { id: "g1111", title: "children1111" },
  //             { id: "g1112", title: "children1112" },
  //           ],
  //         },
  //         { id: "g112", title: "children112" },
  //       ],
  //     },
  //     { id: "g12", title: "children12" },
  //   ],
  // },
  // {
  //   id: "g2",
  //   title: "children2",
  //   children: [
  //     { id: "g20", title: "children20" },
  //     {
  //       id: "g21",
  //       title: "children21",
  //       children: [
  //         {
  //           id: "g210",
  //           title: "children210",
  //           children: [
  //             {
  //               id: "g2100",
  //               title: "children2100",
  //               children: [
  //                 {
  //                   id: "g21000",
  //                   title: "children21000",
  //                   children: [{ id: "g210000", title: "children210000" }],
  //                 },
  //               ],
  //             },
  //           ],
  //         },
  //         // { id: "g211", title: "children211" },
  //         // { id: "g212", title: "children212" },
  //       ],
  //     },
  //     // { id: "g22", title: "children22" },
  //   ],
  // },

  {
    id: "g3",
    title: "children3",
  },
  {
    id: "g4",
    title: "children4",
  },
  {
    id: "g5",
    title: "children5",
  },
  {
    id: "g6",
    title: "children6",
  },
  {
    id: "g7",
    title: "children7",
  },
  {
    id: "g8",
    title: "children8",
  },
  {
    id: "g9",
    title: "children9",
  },
  {
    id: "g10",
    title: "children10",
  },
];

// const menuData = [
//   {
//     title: "111",
//     children: [{ title: "1111" }, { title: "1112" }, { title: "1113" }],
//   },
//   {
//     title: "222",
//     children: [
//       { title: "2221" },
//       { title: "2222", children: [{ title: "22221" }, { title: "22222" }] },
//       { title: "2223" },
//       { title: "2224" },
//     ],
//   },
//   {
//     title: "333",
//     children: [
//       { title: "3331" },
//       { title: "3332" },
//       { title: "3333" },
//       { title: "3334" },
//     ],
//   },
// ];

function App() {
  return (
    <div className="App">
      <div className="App-header">
        <DragNDrop data={menuData} />
      </div>
    </div>
  );
}

export default App;
